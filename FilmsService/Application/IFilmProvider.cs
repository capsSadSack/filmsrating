﻿using FilmsService.Models;

namespace FilmsService.Application
{
    public interface IFilmProvider
    {
        Task<IEnumerable<Film>> GetFilmsByYearAsync(int yearFrom, int yearTo);
        Task<IEnumerable<Film>> GetFilmsByCountryAsync(long countryId);
        Task<IEnumerable<Film>> GetFilmsByStudioAsync(long studioId);
        Task<long> GetCountryIdAsync(string countryName);
    }
}
