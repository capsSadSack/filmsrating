﻿using FilmsService.Application;
using FilmsService.Infrastructure.Configuration;
using FilmsService.Models;
using Microsoft.Data.SqlClient;
using System.Data;

namespace FilmsService.Infrastructure
{
    public class MsSqlFilmProvider : IFilmProvider
    {
        private readonly string _connectionString;

        public MsSqlFilmProvider()
        {
            _connectionString = AppConfiguration.ConnectionString!;
        }


        public async Task<IEnumerable<Film>> GetFilmsByYearAsync(int yearFrom, int yearTo)
        {
            return await Task.Run(() => GetFilmsByYear(yearFrom, yearTo));
        }

        private IEnumerable<Film> GetFilmsByYear(int yearFrom, int yearTo)
        {
            List<Film> films = new List<Film>();

            // create a unique instance of a database connection
            using (var connection = new SqlConnection(_connectionString))
            {
                string procedure = "SP_GetFilmsByYears";

                // Create ADO.NET objects.
                SqlCommand command = new SqlCommand(procedure, connection);
                connection.Open();

                // Configure command and add input parameters.
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@YearFrom", SqlDbType.Int).Value = yearFrom;
                command.Parameters.Add("@YearTo", SqlDbType.Int).Value = yearTo;

                var adapter = new SqlDataAdapter(command);
                var dataSet = new DataSet();

                // populate the data set
                adapter.Fill(dataSet);

                // cast DataRowCollection to DataRow for further mapping
                foreach (var dataRow in dataSet.Tables[0].Rows.Cast<DataRow>())
                {
                    // map DataRows into Film instances
                    films.Add(CreateFilm(dataRow));
                }
            }

            return films;
        }

        private Film CreateFilm(DataRow tableRow)
        {
            Film film = new Film()
            {
                Id = (int)tableRow["Id"],
                Name = (string)tableRow["Name"],
                Country = (string)tableRow["Country"],
                Studio = (string)tableRow["Studio"],
                Year = (int)tableRow["Year"]
            };

            return film;
        }

        public async Task<IEnumerable<Film>> GetFilmsByCountryAsync(long countryId)
        {
            return await Task.Run(() => GetFilmsByCountry(countryId));
        }

        private IEnumerable<Film> GetFilmsByCountry(long countryId)
        {
            List<Film> films = new List<Film>();

            // create a unique instance of a database connection
            using (var connection = new SqlConnection(_connectionString))
            {
                string procedure = "SP_GetFilmsByCountry";

                // Create ADO.NET objects.
                SqlCommand command = new SqlCommand(procedure, connection);
                connection.Open();

                // Configure command and add input parameters.
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@countryId", SqlDbType.Int).Value = countryId;

                var adapter = new SqlDataAdapter(command);
                var dataSet = new DataSet();

                // populate the data set
                adapter.Fill(dataSet);

                // cast DataRowCollection to DataRow for further mapping
                foreach (var dataRow in dataSet.Tables[0].Rows.Cast<DataRow>())
                {
                    // map DataRows into Film instances
                    films.Add(CreateFilm(dataRow));
                }
            }

            return films;
        }

        public async Task<IEnumerable<Film>> GetFilmsByStudioAsync(long studioId)
        {
            return await Task.Run(() => GetFilmsByStudio(studioId));
        }

        private IEnumerable<Film> GetFilmsByStudio(long studioId)
        {
            List<Film> films = new List<Film>();

            // create a unique instance of a database connection
            using (var connection = new SqlConnection(_connectionString))
            {
                string procedure = "SP_GetFilmsByStudio";

                // Create ADO.NET objects.
                SqlCommand command = new SqlCommand(procedure, connection);
                connection.Open();

                // Configure command and add input parameters.
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@countryId", SqlDbType.Int).Value = studioId;

                var adapter = new SqlDataAdapter(command);
                var dataSet = new DataSet();

                // populate the data set
                adapter.Fill(dataSet);

                // cast DataRowCollection to DataRow for further mapping
                foreach (var dataRow in dataSet.Tables[0].Rows.Cast<DataRow>())
                {
                    // map DataRows into Film instances
                    films.Add(CreateFilm(dataRow));
                }
            }

            return films;
        }

        public async Task<long> GetCountryIdAsync(string countryName)
        {
            return await Task.Run(() => GetCountryId(countryName));
        }

        private long GetCountryId(string countryName)
        {
            long countryId;

            // create a unique instance of a database connection
            using (var connection = new SqlConnection(_connectionString))
            {
                string procedure = "SP_GetCountryIdByName";

                // Create ADO.NET objects.
                SqlCommand command = new SqlCommand(procedure, connection);
                connection.Open();

                // Configure command and add input parameters.
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@countryName", SqlDbType.VarChar).Value = countryName;
                command.Parameters.Add("@countryId", SqlDbType.Int).Direction = ParameterDirection.Output;

                command.ExecuteNonQuery();

                // read output value from @countryId
                countryId = Convert.ToInt32(command.Parameters["@countryId"].Value);
            }

            return countryId;
        }
    }
}
