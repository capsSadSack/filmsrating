﻿namespace FilmsService.Infrastructure.Configuration
{
    public static class AppConfiguration
    {
        public static string? ConnectionString { get; set; }

        public static void ConfigureConfig(this IServiceCollection services, IConfiguration configuration)
        {
            var configurationSection = configuration
                .GetRequiredSection("ConnectionStrings")
                .GetValue<string>("localMsSqlServerDb");

            ConnectionString = configurationSection!;
        }
    }
}
