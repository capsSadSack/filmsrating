﻿using FluentValidation;

namespace FilmsService.Api.Validation
{
    public class YearRangeValidator : AbstractValidator<(int, int)>
    {
        public YearRangeValidator()
        {
            RuleFor(x => x.Item1)
                .NotEmpty()
                .GreaterThanOrEqualTo(0)
                .WithMessage("Incorrect year from.");

            RuleFor(x => x.Item2)
                .NotEmpty()
                .GreaterThanOrEqualTo(0)
                .WithMessage("Incorrect year to.");

            RuleFor(x => new { x.Item1, x.Item2 })
                .NotEmpty()
                .Must(x => x.Item2 >= x.Item1)
                .WithMessage("Year to must be greater than year from.");
        }
    }
}
