﻿using FluentValidation;
using System.Text.RegularExpressions;

namespace FilmsService.Api.Validation
{
    public class CountryNameValidator : AbstractValidator<string>
    {
        public CountryNameValidator()
        {
            RuleFor(x => x)
                .NotEmpty()
                .MaximumLength(50)
                .Must(x => Regex.IsMatch(x, @"^[a-zA-Z- ]+$"))
                .WithMessage("Wrong country name.");
        }
    }
}
