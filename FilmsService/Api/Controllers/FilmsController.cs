using FilmsService.Application;
using FilmsService.Models;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;

namespace FilmsService.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FilmsController : ControllerBase
    {
        private readonly ILogger<FilmsController> _logger;
        private readonly IFilmProvider _filmProvider;
        private readonly IValidator<(int, int)> _yearRangeValidator;
        private readonly IValidator<string> _countryNameValidator;


        public FilmsController(ILogger<FilmsController> logger, IFilmProvider filmProvider,
            IValidator<(int, int)> yearRangeValidator,
            IValidator<string> countryNameValidator)
        {
            _logger = logger
                ?? throw new ArgumentNullException(nameof(logger));
            _filmProvider = filmProvider
                ?? throw new ArgumentNullException(nameof(filmProvider));

            _yearRangeValidator = yearRangeValidator
                ?? throw new ArgumentNullException(nameof(yearRangeValidator));
            _countryNameValidator = countryNameValidator
                ?? throw new ArgumentNullException(nameof(countryNameValidator));
        }


        [HttpGet]
        [Route("GetFilmsByYear")]
        public async Task<ActionResult> GetFilmsByYearAsync(int yearFrom, int yearTo)
        {
            _logger.LogInformation($"Method {nameof(GetFilmsByYearAsync)} " +
                $"was called with parameters: " +
                $"{nameof(yearFrom)}: {yearFrom}," +
                $"{nameof(yearTo)}: {yearTo}.");

            var validationResult = await _yearRangeValidator.ValidateAsync((yearFrom, yearTo), HttpContext.RequestAborted);
            if (!validationResult.IsValid)
            {
                _logger.LogError($"{validationResult}");
                return BadRequest(validationResult);
            }

            IEnumerable<Film> films =  await _filmProvider.GetFilmsByYearAsync(yearFrom, yearTo);
            return Ok(films);
        }

        [HttpGet]
        [Route("GetFilmsByCountry")]
        public async Task<ActionResult> GetFilmsByCountryAsync(string country)
        {
            _logger.LogInformation($"Method {nameof(GetFilmsByCountryAsync)} " +
                $"was called with parameters: " +
                $"{nameof(country)}: {country},");

            var validationResult = await _countryNameValidator.ValidateAsync(country, HttpContext.RequestAborted);
            if (!validationResult.IsValid)
            {
                _logger.LogError($"{validationResult}");
                return BadRequest(validationResult);
            }

            long countryId = await _filmProvider.GetCountryIdAsync(country);

            IEnumerable<Film> films = await _filmProvider.GetFilmsByCountryAsync(countryId);
            return Ok(films);
        }
    }
}
