﻿namespace FilmsService.Models
{
    public class Film
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public string Studio { get; set; }
        public int Year { get; set; }
    }
}
