﻿CREATE PROCEDURE [dbo].[SP_GetCountryIdByName]
	@countryName VARCHAR(50),
	@countryId INT OUTPUT
AS
	SELECT TOP(1) @countryId = [dbo].[Country].[Id]
		FROM [dbo].[Country]
		WHERE UPPER([dbo].[Country].[Name]) = UPPER(@countryName)
RETURN @countryId
