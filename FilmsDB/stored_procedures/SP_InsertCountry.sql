﻿CREATE PROCEDURE [dbo].[SP_InsertCountry]
	@name VARCHAR(30)
AS
	INSERT INTO [dbo].[Country] ([Name])
	VALUES (@name);
RETURN 1
