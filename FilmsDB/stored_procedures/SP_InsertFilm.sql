﻿CREATE PROCEDURE [dbo].[SP_InsertFilm]
	@name VARCHAR(50),
	@countryId int,
	@studioId int,
	@year int
AS
	INSERT INTO [dbo].[Film] ([Name], [CountryId], [StudioId], [Year])
	VALUES (@name, @countryId, @studioId, @year);
RETURN 1
