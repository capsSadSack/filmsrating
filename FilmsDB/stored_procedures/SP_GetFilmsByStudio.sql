﻿CREATE PROCEDURE [dbo].[SP_GetFilmsByStudio]
	@studioId int
AS
	SELECT
        Film.Id AS Id,
        Film.Name AS Name,
        Film.Year AS Year,
        Country.Name AS Country,
        Studio.Name AS Studio
    FROM Film
        LEFT JOIN Country ON Film.CountryId = Country.Id
        LEFT JOIN Studio ON Film.StudioId = Studio.Id
    WHERE Film.StudioId = @studioId
