﻿CREATE TABLE [dbo].[Film]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	[Name] VARCHAR(50) NOT NULL,
	[CountryId] INT NOT NULL,
	[StudioId] INT NOT NULL,
	[Year] INT NOT NULL,

	CONSTRAINT FK_Film_Country FOREIGN KEY ([CountryId]) REFERENCES [dbo].[Country] ([Id]),
	CONSTRAINT FK_Film_Studio  FOREIGN KEY ([StudioId])  REFERENCES [dbo].[Studio] ([Id])
)
